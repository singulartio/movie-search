import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from './../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getFilms(searchTerm: string): Promise<any> {
    let params = new HttpParams();
    params = params.append('term', searchTerm);

    return this.http
      .get(environment.apiUrls.films, { params })
      .toPromise()
      .then(response => {
        return response;
      })
      .catch((error: any) => {
        console.log(error);
      });
  }
}
